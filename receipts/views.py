from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


@login_required
def receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)

    context = {"receipts": receipts}

    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {"form": form}

    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    expense = ExpenseCategory.objects.filter(owner=request.user)

    context = {"expense": expense}

    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)

    context = {"account": account}

    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")

    else:
        form = CategoryForm()

    context = {"form": form}

    return render(request, "receipts/createcategory.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")

    else:
        form = AccountForm()

    context = {"form": form}

    return render(request, "receipts/createaccount.html", context)


# Create your views here.
